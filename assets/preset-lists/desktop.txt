# User-maintained list: Desktop Browsers 
# ================================================= 
# Source: https://techpatterns.com/downloads/firefox/useragentswitcher.xml 
# 
# This list may be edited by visiting (GitLab login required) 
# https://gitlab.com/ntninja/user-agent-switcher/edit/master/assets/preset-lists/desktop.txt
# and submitting a merge request. 
#
Arora 0.10.2 (BSD/Haiku) [Desktop]: Mozilla/5.0 (Unknown; U; UNIX BSD/SYSV system; C -) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.10.2
Arora 0.11 - WebKit [Desktop]: Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.10.1
Arora 0.8.0 - (Windows) [Desktop]: Mozilla/5.0 (Windows; U; ; en-NZ) AppleWebKit/527  (KHTML, like Gecko, Safari/419.3) Arora/0.8.0
Avant Browser - MSIE 7 (Win XP) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser; Avant Browser; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30)
Beamrise - (Win 7) - Webkit 535.8 [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.8 (KHTML, like Gecko) Beamrise/17.2.0.9 Chrome/17.0.939.0 Safari/535.8
Chrome 19.0 (FreeBSD 64) [Desktop]: Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/536.5 (KHTML like Gecko) Chrome/19.0.1084.56 Safari/536.5
Chrome 20.0 (CrOS) [Desktop]: Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11
Chrome 22.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.56 Safari/537.4
Chrome 22.0 (FreeBSD 64) [Desktop]: Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.79 Safari/537.4
Chrome 26.0 (OS X 10_8_4 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.31 (KHTML like Gecko) Chrome/26.0.1410.63 Safari/537.31
Chrome 27.0 (NetBSD) [Desktop]: Mozilla/5.0 (X11; NetBSD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36
Chrome 28.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1478.0 Safari/537.36
Chrome 28.0 (OS X 10_8_3 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 1083) AppleWebKit/537.36 (KHTML like Gecko) Chrome/28.0.1469.0 Safari/537.36
Chrome 28.0 (Win 7 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/28.0.1469.0 Safari/537.36
Chrome 28.0 (Win 8 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/28.0.1469.0 Safari/537.36
Chrome 32.0 (OS X 10_9_0 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1664.3 Safari/537.36
Chrome 32.0 (Win 8 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1667.0 Safari/537.36
Chrome 36.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/36.0.1985.125 Safari/537.36
Chrome 36.0 (CrOS) [Desktop]: Mozilla/5.0 (X11; CrOS x86_64 5841.83.0) AppleWebKit/537.36 (KHTML like Gecko) Chrome/36.0.1985.138 Safari/537.36
Chrome 36.0 (OpenBSD) [Desktop]: Mozilla/5.0 (X11; OpenBSD i386) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36
Chrome 36.0 (OS X 10_9_2 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1944.0 Safari/537.36
Chrome 36.0 (Win 8 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.67 Safari/537.36
Chrome 37.0 (Win 8.1 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36
Chrome 39.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2166.2 Safari/537.36
Chrome 41.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36
Chrome 41.0 (OS X 10_10_1) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36
Chrome 41.0 (Win 7 - 32 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36
Chrome 43.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.93 Safari/537.36
Chrome 44.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.130 Safari/537.36
Chrome 45.0 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36
Chrome 45.0 (Win Vista - 32 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36
Chrome 51.0 (OS X 10_11_5) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36
Chrome 51.0 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36
Chrome 52.0 (Fedora 64 bit) [Desktop]: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
Chrome 52.0 (OS X 10_10_1) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
Chrome 55.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2876.0 Safari/537.36
Chrome 55.0 (OS X 10_10_5) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2859.0 Safari/537.36
Chrome 55.0 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2869.0 Safari/537.36
Chrome 57.0 AOL (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 AOL/11.0 AOLBUILD/11.0.1305 Safari/537.36
Chrome 60.0 (OS X 10_10_1) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36
Chrome 61.0 (OS X 10_10_5) Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.49 Safari/537.36
Chrome 62.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3187.0 Safari/537.366
Chrome 62.0 (Fedora s64 bit) [Desktop]: Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3178.0 Safari/537.36
Chrome 62.0 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3191.0 Safari/537.36
Chromium 20.0 (NetBSD) [Desktop]: Mozilla/5.0 (X11; NetBSD x86; en-us) AppleWebKit/666.6+ (KHTML, like Gecko) Chromium/20.0.0000.00 Chrome/20.0.0000.00 Safari/666.6+
Chromium 25.0 (Ubuntu 32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.22 (KHTML like Gecko) Ubuntu Chromium/25.0.1364.160 Chrome/25.0.1364.160 Safari/537.22
Chromium 33.0 (Ubuntu 64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/33.0.1750.152 Chrome/33.0.1750.152 Safari/537.36
Chromium 51.0 (Ubuntu 32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36
Chromium 60.0 (Ubuntu 32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/60.0.3112.78 Chrome/60.0.3112.78 Safari/537.36
Dillo 3.0 [Desktop]: Mozilla/4.0 (compatible; Dillo 3.0)
Edge (Microsoft) 12.0 (EdgeHTML) Chrome 39 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0
Edge (Microsoft) 12.1 (EdgeHTML) Chrome 42 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240
Edge (Microsoft) 14.14 (EdgeHTML) Chrome 51 (Win 10 - 64 bit) [Desktop]: Mozilla/5.0 (MSIE 9.0; Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14931
Edge (Microsoft) 15.15 (EdgeHTML) Chrome 52 (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063
Epiphany 2.30.0 (FreeBSD) [Desktop]: Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/535.22+ (KHTML, like Gecko) Chromium/17.0.963.56 Chrome/17.0.963.56 Safari/535.22+ Epiphany/2.30.6
Epiphany 2.30.0 (OpenBSD) [Desktop]: Mozilla/5.0 (X11; U; OpenBSD arm; en-us) AppleWebKit/531.2  (KHTML, like Gecko) Safari/531.2  Epiphany/2.30.0
Epiphany 3.24 (Ubuntu - Webkit 604.1) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/604.1 (KHTML, like Gecko) Version/11.0 Safari/604.1 Ubuntu/17.04 (3.24.1-0ubuntu1) Epiphany/3.24.1
Epiphany 3.8.2 - WebKit (537.32) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.32 (KHTML, like Gecko) Chromium/25.0.1349.2 Chrome/25.0.1349.2 Safari/537.32 Epiphany/3.8.2
Epiphany - WebKit (528.5) [Desktop]: Mozilla/5.0 (X11; U; Linux i686; en-us) AppleWebKit/528.5  (KHTML, like Gecko, Safari/528.5 ) lt-GtkLauncher
Firefox 16.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:16.0) Gecko/20100101 Firefox/16.0
Firefox 16.0 (NetBSD 64) [Desktop]: Mozilla/5.0 (X11; NetBSD amd64; rv:16.0) Gecko/20121102 Firefox/16.0
Firefox 19.0 (Slackware 13 32 bit) [Desktop]: Mozilla/5.0 (X11; U; Linux i686; rv:19.0) Gecko/20100101 Slackware/13 Firefox/19.0
Firefox 20.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:20.0) Gecko/20100101 Firefox/20.0
Firefox 20.0 (OS X 10.7 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:20.0) Gecko/20100101 Firefox/20.0
Firefox 20.0 (Ubuntu 64 bit) [Desktop]: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:20.0) Gecko/20100101 Firefox/20.0
Firefox 21.0 (OS X 10.8 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0
Firefox 21.0 (Win 7 32) [Desktop]: Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130401 Firefox/21.0
Firefox 25.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:25.0) Gecko/20100101 Firefox/25.0
Firefox 25.0 (OS X 10.6 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:25.0) Gecko/20100101 Firefox/25.0
Firefox 25.0 (Win 7 64) [Desktop]: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0
Firefox 28.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0
Firefox 28.0 (OpenBSD 64) [Desktop]: Mozilla/5.0 (X11; OpenBSD amd64; rv:28.0) Gecko/20100101 Firefox/28.0
Firefox 29.0 (Win 7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/29.0
Firefox 30.0 (NetBSD 64) [Desktop]: Mozilla/5.0 (X11; NetBSD amd64; rv:30.0) Gecko/20100101 Firefox/30.0
Firefox 30.0 (OpenBSD 64) [Desktop]: Mozilla/5.0 (X11; OpenBSD amd64; rv:30.0) Gecko/20100101 Firefox/30.0
Firefox 31.0 (Win XP) [Desktop]: Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0
Firefox 32.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0
Firefox 35.0 (FreeBSD 64) [Desktop]: Mozilla/5.0 (X11; FreeBSD amd64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36
Firefox 35.0 (OS X 10.9 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0
Firefox 35.0 (Ubuntu 64 bit) [Desktop]: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0
Firefox 35.0 (Win 7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:35.0) Gecko/20100101 Firefox/35.0
Firefox 36.0 (CentOS 64 bit) [Desktop]: Mozilla/5.0 (X11; CentOS; Linux x86_64; rv:36.0) Gecko/20100101 Firefox/36.0
Firefox 36.0 (Win 8.1 32 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0
Firefox 38.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0
Firefox 39.0 (Win 8.0 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.2; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0
Firefox 40.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:40.0) Gecko/20100101 Firefox/40.0
Firefox 40.0 (OS X 10.10 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0
Firefox 40.0 (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0
Firefox 40.0 (Win Vista) [Desktop]: Mozilla/5.0 (Windows NT 6.0; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0
Firefox 43.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:43.0) Gecko/20100101 Firefox/43.0
Firefox 46.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:46.0) Gecko/20100101 Firefox/46.0
Firefox 47.0 (OS X 10.9 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:47.0) Gecko/20100101 Firefox/47.0
Firefox 47.0 (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0
Firefox 49.0 (32 bit) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:49.0) Gecko/20100101 Firefox/49.0
Firefox 49.0 (Fedora 64 bit) [Desktop]: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0
Firefox 49.0 (OS X 10.12 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:49.0) Gecko/20100101 Firefox/49.0" appname="" appversion="" platform="" vendor="" vendorsub=""/>
Firefox 49.0 (Ubuntu 64 bit) [Desktop]: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:49.0) Gecko/20100101 Firefox/49.0
Firefox 52.0 (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0
Firefox 54.0 (FreeBSD 64) [Desktop]: Mozilla/5.0 (X11; FreeBSD amd64; rv:54.0) Gecko/20100101 Firefox/54.0
Firefox 55.0 (64 bit) [Desktop]: Mozilla/5.0 (X11; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0
Firefox 55.0 (OS X 10.13 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:55.0) Gecko/20100101 Firefox/55.0" appname="" appversion="" platform="" vendor="" vendorsub=""/>
Firefox 55.0 (Ubuntu 64 bit) [Desktop]: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0
Firefox 57.0 (Win 10 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:57.0) Gecko/20100101 Firefox/57.0
Galeon 2.0.6 (Gentoo) [Desktop]: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.16) Gecko/20080716 (Gentoo) Galeon/2.0.6
Galeon 2.0.6 (Ubuntu) [Desktop]: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Galeon/2.0.6 (Ubuntu 2.0.6-2)
Iceape (SeaMonkey) 2.0.8 (Debian) [Desktop]: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.13) Gecko/20100916 Iceape/2.0.8
Iceweasel (Firefox) 19.0 (Debian 64) [Desktop]: Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0 Iceweasel/19.0.2
Iceweasel (Firefox) 38.0 (Debian 64) [Desktop]: Mozilla/5.0 (X11; Linux x86_64; rv:38.0) Gecko/20100101 Firefox/38.0 Iceweasel/38.2.1
iTunes 4.2 (OS X 10.2 PPC) [Desktop]: iTunes/4.2 (Macintosh; U; PPC Mac OS X 10.2)" appname="" appversion="" platform="" vendor="" vendorsub=""/>
iTunes 9.0.2 (Windows) [Desktop]: iTunes/9.0.2 (Windows; N)
iTunes 9.0.3 (OS X 10_6_2) [Desktop]: iTunes/9.0.3 (Macintosh; U; Intel Mac OS X 10_6_2; en-ca)" appname="" appversion="" platform="" vendor="" vendorsub=""/>
Konqueror 4.14 - khtml [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.21 (KHTML, like Gecko) konqueror/4.14.10 Safari/537.21
Konqueror 4.1 - khtml (DragonFly) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.1; DragonFly) KHTML/4.1.4 (like Gecko)
Konqueror 4.1 - khtml (OpenBSD) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.1; OpenBSD) KHTML/4.1.4 (like Gecko)
Konqueror 4.3 - khtml (Fedora 11) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.3; Linux) KHTML/4.3.1 (like Gecko) Fedora/4.3.1-3.fc11
Konqueror 4.3 - khtml (Slackware 13) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.2; Linux) KHTML/4.2.4 (like Gecko) Slackware/13.0
Konqueror 4.4 - khtml (Fedora 12) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.4; Linux) KHTML/4.4.1 (like Gecko) Fedora/4.4.1-1.fc12
Konqueror 4.4 - khtml (Kubuntu) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.4; Linux 2.6.32-22-generic; X11; en_US) KHTML/4.4.3 (like Gecko) Kubuntu
Konqueror 4.4 - khtml (Kubuntu) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.4; Linux 2.6.32-22-generic; X11; en_US) KHTML/4.4.3 (like Gecko) Kubuntu
Konqueror 4.5.4 - khtml (FreeBSD) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.5; FreeBSD) KHTML/4.5.4 (like Gecko)
Konqueror 4.5.4 - khtml (NetBSD 5.0.2) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.5; NetBSD 5.0.2; X11; amd64; en_US) KHTML/4.5.4 (like Gecko)
Konqueror 4.5 (Win XP - KDE native) [Desktop]: Mozilla/5.0 (compatible; Konqueror/4.5; Windows) KHTML/4.5.4 (like Gecko)
Konqueror 4.8 - khtml (Debian) [Desktop]: Mozilla/5.0 (X11; Linux 3.8-6.dmz.1-liquorix-686) KHTML/4.8.4 (like Gecko) Konqueror/4.8
Konqueror 4.9 - khtml [Desktop]: Mozilla/5.0 (X11; Linux) KHTML/4.9.1 (like Gecko) Konqueror/4.9
Maxthon 3.0 (Webkit) (Vista) [Desktop]: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.1 (KHTML, like Gecko) Maxthon/3.0.8.2 Safari/533.1
Maxthon 4.0 (Chrome 22) (Win7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML like Gecko) Maxthon/4.0.0.2000 Chrome/22.0.1229.79 Safari/537.1
Maxthon 4.4 (Chrome 30) (Win7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/4.4.6.1000 Chrome/30.0.1599.101 Safari/537.36
Maxthon 4.5.2 (AppleWebKit 600.8) (OS X Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Maxthon/4.5.2
Maxthon 5.0 (Chrome 47) (Win7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Maxthon/5.0.4.3000 Chrome/47.0.2526.73 Safari/537.36
Midori 0.1.10 (Webkit 531) [Desktop]: Midori/0.1.10 (X11; Linux i686; U; en-us) WebKit/(531).(2)
Mozilla 1.9.0 (Debian) [Desktop]: Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.3) Gecko/2008092814 (Debian-3.0.1-1)
Mozilla 1.9a3pre [Desktop]: Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9a3pre) Gecko/20070330
MSIE 10.6 - (Win 7 32) [Desktop]: Mozilla/5.0 (compatible; MSIE 10.6; Windows NT 6.1; Trident/5.0; InfoPath.2; SLCC1; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; .NET CLR 2.0.50727) 3gpp-gba UNTRUSTED/1.0
MSIE 10 - compat mode (Win 7 64) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/6.0)
MSIE 10 - standard mode (Win 7 64) [Desktop]: Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)
MSIE 11.0 (compatibility mode IE 7)- (Win 8.1 32) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.3; Trident/7.0; .NET4.0E; .NET4.0C)
MSIE 11.0 touch (Win 10 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; Touch; MALNJS; rv:11.0) like Gecko
MSIE 11.0 (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; MATBJS; rv:11.0) like Gecko
MSIE 11.0 - (Win 7 64) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko
MSIE 11.0 - (Win 8.1 32) [Desktop]: Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko
MSIE 6 (Win XP) [Desktop]: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)" appcodename="" appname="Microsoft Internet Explorer" appversion="4.0 (Compatible; MSIE 6.0; Windows NT 5.1)" platform="" vendor="" vendorsub=""/>
MSIE 7 (Vista) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)" appcodename="Mozilla" appname="Microsoft Internet Explorer" appversion="4.0 (compatible; MSIE 7.0; Windows NT 6.0)" platform="Win32" vendor="" vendorsub=""/>
MSIE 8 - compat mode (Vista) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/4.0)" appcodename="" appname="" appversion="" platform="win32" vendor="" vendorsub=""/>
MSIE 8 - standard mode (Vista) [Desktop]: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)" appcodename="" appname="" appversion="" platform="win32" vendor="" vendorsub=""/>
MSIE 8 - standard mode (Win 7) [Desktop]: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)
MSIE 8 - standard mode (Win XP) [Desktop]: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)
MSIE 9 - compat mode (Vista) [Desktop]: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Trident/5.0)
MSIE 9 - standard mode (NT 6.2 32 Win 8) [Desktop]: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.2; Trident/5.0)
MSIE 9 - standard mode (NT 6.2 64 Win 8) [Desktop]: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.2; WOW64; Trident/5.0)
MSIE 9 - standard mode (Win 7) [Desktop]: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)
MSIE 9 - standard mode (with Zune plugin) (NT 6.1 Win 7) [Desktop]: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; Media Center PC 6.0; InfoPath.3; MS-RTC LM 8; Zune 4.7)
MxBrowser (Chrome 30)- (Win XP) [Desktop]: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) MxBrowser/4.5.10.7000 Chrome/30.0.1551.0 Safari/537.36
Namoroka 3.6.15 (Firefox) (NetBSD) [Desktop]: Mozilla/5.0 (X11; U; NetBSD amd64; en-US; rv:1.9.2.15) Gecko/20110308 Namoroka/3.6.15
NetSurf 1.2 (NetBSD) [Desktop]: NetSurf/1.2 (NetBSD; amd64)
Omniweb 622.8 (OS X 10_5_6 Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-US) AppleWebKit/528.16 (KHTML, like Gecko, Safari/528.16) OmniWeb/v622.8.0
Omniweb 622.8 (OS X Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US) AppleWebKit/528.16 (KHTML, like Gecko, Safari/528.16) OmniWeb/v622.8.0.112941
Opera 11.52 (id as 9.8) (OS X Intel) [Desktop]: Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; fr) Presto/2.9.168 Version/11.52
Opera 12.10 (FreeBSD) [Desktop]: Opera/9.80 (X11; FreeBSD 8.1-RELEASE i386; Edition Next) Presto/2.12.388 Version/12.10
Opera 12.16 (id as 9.8, last presto) [Desktop]: Opera/9.80 (X11; Linux i686) Presto/2.12.388 Version/12.16
Opera 12.16 (id as 9.8) (Win 7) [Desktop]: Opera/9.80 (Windows NT 6.1; WOW64) Presto/2.12.388 Version/12.16
Opera 14.0 (Chrome 27) (Win 7) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.12 Safari/537.36 OPR/14.0.1116.4
Opera 15.0 (Chrome 28) (Win 7) [Desktop]: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36 OPR/15.0.1147.24 (Edition Next)
Opera 18.0 (Chrome 31) (Win 8.1) [Desktop]: Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.57 Safari/537.36 OPR/18.0.1284.49
Opera 19.0 (Chrome 32) (Win 7) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36 OPR/19.0.1326.56
Opera 20.0 (Chrome 33) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.166 Safari/537.36 OPR/20.0.1396.73172
Opera 20.0 (Chrome 33) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.166 Safari/537.36 OPR/20.0.1396.73172
Opera 20.0 (Chrome 33) (Win 7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.154 Safari/537.36 OPR/20.0.1387.91
Opera 28.0 (Blink) (OS X 10_10_2 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36 OPR/28.0.1750.51
Opera 28.0 (Chrome 41) (Win 8 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.76 Safari/537.36 OPR/28.0.1750.40
Opera 29.0 (Blink) (OS X 10_10_2 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.82 Safari/537.36 OPR/29.0.1795.41
Opera 31.0 (Chrome 44) (Win 7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36 OPR/31.0.1889.174
Opera 32.0 (Chrome 45) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36 OPR/32.0.1948.25
Opera 36.0 (Chrome 49) (Win 10 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36 OPR/36.0.2130.46
Opera 40.0 (Chrome 53) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.101 Safari/537.36 OPR/40.0.2308.62
Opera 47.0 (Chrome 60) (Win 10 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.78 Safari/537.36 OPR/47.0.2631.55
Palemoon 27.4 (Firefox 45.9) (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; rv:45.9) Gecko/20100101 Goanna/3.2 Firefox/45.9 PaleMoon/27.4.0
Puffin 4.8 (Chrome 30) [Desktop]: Mozilla/5.0 (X11; U; Linux x86_64; en-us) AppleWebKit/537.36 (KHTML, like Gecko)  Chrome/30.0.1599.114 Safari/537.36 Puffin/4.8.0.2965AT
QupZilla 1.7.0 (FreeBSD) [Desktop]: Mozilla/5.0 (Unknown; UNIX BSD/SYSV system) AppleWebKit/538.1 (KHTML, like Gecko) QupZilla/1.7.0 Safari/538.1
QupZilla 1.8 (Webkit 538.2) [Desktop]: Mozilla/5.0 (X11; Linux i686) AppleWebKit/538.1 (KHTML, like Gecko) QupZilla/1.8.6 Safari/538.1
QupZilla 1.9 (Webkit 538.1) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/538.1 (KHTML, like Gecko) QupZilla/1.9.0 Safari/538.1
Safari 10.0 602.1 (OS X 10_10_5) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Safari/602.1.50
Safari 10.1 601.7 (OS X 10_11_6) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/601.7.8 (KHTML, like Gecko) Version/10.1 Safari/603.1.30
Safari 531.21 (OS X 10_6_2 Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_2; en-us) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10
Safari 533.17 (Server 2003/64 bit) [Desktop]: Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8
Safari 533.19 (OS X 10_6_5 Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_5; de-de) AppleWebKit/534.15  (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4
Safari 533.19 (Win 7) [Desktop]: Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5
Safari 533.20 (OS X 10_6_6 Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_6; en-us) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27
Safari 534.20 (OS X 10_7 Intel) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_7; en-us) AppleWebKit/534.20.8 (KHTML, like Gecko) Version/5.1 Safari/534.20.8
Safari 534.55 (OS X 10_7_3 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10
Safari 534.57 (5.1.7) (OS X 10_6_8 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2
Safari 536.26 (6) (OS X 10_7_5 Intel) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/536.26.17 (KHTML like Gecko) Version/6.0.2 Safari/536.26.17
Safari 6.0 (Win 8) [Desktop]: Mozilla/5.0 (Windows; U; Windows NT 6.2; es-US ) AppleWebKit/540.0 (KHTML like Gecko) Version/6.0 Safari/8900.00
Safari 7.0 537.75 (OS X 10_9_3) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A
Safari 7.0 (Win 7) [Desktop]: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.71 (KHTML like Gecko) WebVideo/1.0.1.10 Version/7.0 Safari/537.71
Safari 7 537.78 (OS X 10_9_5) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.78.1 (KHTML like Gecko) Version/7.0.6 Safari/537.78.1
Safari 8.0 600.8 (OS X 10_10_5) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/600.8.9 (KHTML, like Gecko) Version/8.0.8 Safari/600.8.9
Safari 9.0 601.1.56 (OS X 10_11) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56" appcodename="Mozilla" appname="Netscape" appversion="5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56" platform="MacIntel" vendor="Apple Computer, Inc." vendorsub=""/>
Seamonkey 1.1.8 (Mozilla) (SunOS 32bit) [Desktop]: Mozilla/5.0 (X11; U; SunOS i86pc; en-US; rv:1.8.1.12) Gecko/20080303 SeaMonkey/1.1.8
Seamonkey 2.25 (Firefox/28.0) (FreeBSD) [Desktop]: Mozilla/5.0 (X11; FreeBSD i386; rv:28.0) Gecko/20100101 Firefox/28.0 SeaMonkey/2.25
SeaMonkey 2.35 (Firefox 38) [Desktop]: Mozilla/5.0 (Windows NT 5.1; rv:38.0) Gecko/20100101 Firefox/38.0 SeaMonkey/2.35
SeaMonkey 2.46 (Firefox 49) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:49.0) Gecko/20100101 Firefox/49.0 SeaMonkey/2.46
SeaMonkey 2.7.1 (OS X 10.5 - Mozilla) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.5; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1
SeaMonkey 2.7 (Firefox 10) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:10.0.1) Gecko/20100101 Firefox/10.0.1 SeaMonkey/2.7.1
SeaMonkey 2.9 (Firefox 12) [Desktop]: Mozilla/5.0 (X11; Linux i686; rv:12.0) Gecko/20120502 Firefox/12.0 SeaMonkey/2.9.1
SeaMonkey (Mozilla) 2.33 (Win Vista) [Desktop]: Mozilla/5.0 (Windows NT 6.0; rv:36.0) Gecko/20100101 Firefox/36.0 SeaMonkey/2.33.1
SeaMonkey (Mozilla) 2.9 (Win7 64 bit) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20120422 Firefox/12.0 SeaMonkey/2.9
Shadowfox 7.0 (Firefox 7) [Desktop]: Mozilla/5.0 (X11; U; Linux x86_64; us; rv:1.9.1.19) Gecko/20110430 shadowfox/7.0 (like Firefox/7.0
Silk 1.0.13 (AppleWebKit 533.16) 2.9 (Mac OS X 10_6_3) [Desktop]: Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_3; en-us; Silk/1.0.13.81_10003810) AppleWebKit/533.16 (KHTML, like Gecko) Version/5.0 Safari/533.16 Silk-Accelerated=true
Swiftfox 3.6.3 [Desktop]: Mozilla/5.0 (X11; U; Linux i686; it; rv:1.9.2.3) Gecko/20100406 Firefox/3.6.3 (Swiftfox)
UBrowser 5.6 2.33 (chrome/webkit) (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 UBrowser/5.6.13705.206 Safari/537.36
Uzbl (Webkit 1.3) [Desktop]: Uzbl (Webkit 1.3) (Linux i686 [i686])
Vivaldi 1.0.162 (Chrome 41) (Mac OS X 10_10_3) [Desktop]: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.105 Safari/537.36 Vivaldi/1.0.162.9
Vivaldi 1.0.344 (Chrome 47) [Desktop]: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Vivaldi/1.0.344.37
Vivaldi 1.0.94 (Blink) (Win 7) [Desktop]: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.89 Vivaldi/1.0.94.2 Safari/537.36
Vivaldi 1.4 (Blink) (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.90 Safari/537.36 Vivaldi/1.4.589.11
Vivaldi 1.92 (Blink) (Win 10) [Desktop]: Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.91 Safari/537.36 Vivaldi/1.92.917.39
Yowser 2.5 (Blink - Chrome 56) (Win XP) [Desktop]: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 YaBrowser/17.3.0.1785 Yowser/2.5 Safari/537.36
